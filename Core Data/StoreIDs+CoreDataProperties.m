//
//  StoreIDs+CoreDataProperties.m

//
//  Created by Rahul Sharma on 12/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "StoreIDs+CoreDataProperties.h"

@implementation StoreIDs (CoreDataProperties)

+ (NSFetchRequest<StoreIDs *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"StoreIDs"];
}

@dynamic documentid;
@dynamic groupid;

@end
