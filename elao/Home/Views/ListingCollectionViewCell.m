//
//  ListingCollectionViewCell.m

//
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ListingCollectionViewCell.h"
#import "TinderGenericUtility.h"
#import "UIImageView+WebCache.h"

@implementation ListingCollectionViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor=[[UIColor whiteColor] CGColor];
    self.contentView.backgroundColor =[UIColor clearColor];
    self.postedImageOutlet.contentMode = UIViewContentModeScaleAspectFill;
    [self layoutIfNeeded];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.postedImageOutlet.image = nil;
    self.postedImageOutlet.clipsToBounds = YES ;
    
}


/**
 Set Products on homescreen.

 @param product productModel.
 */
-(void)setProducts:(ProductDetails *)product;
{
    
    if(product.isPromoted)
    {
        self.featuredView.hidden = NO ;
    }
    else
    {
        self.featuredView.hidden = YES ;
    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    [self.postedImageOutlet setImageWithURL:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    // request image

     NSString *productMainUrl =  [product.mainUrl stringByReplacingOccurrencesOfString:@"upload/" withString:@"upload/c_fit,h_500,q_40,w_500/"];
    
    if([manager diskImageExistsForURL:[NSURL URLWithString:productMainUrl]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.postedImageOutlet setImage: [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:productMainUrl]];
        });
    }
    else
    {
        [self.postedImageOutlet setImageWithURL:[NSURL URLWithString:productMainUrl] placeholderImage:[UIImage imageNamed:@""] options:0 completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType,NSURL *imageURL){
            dispatch_async(dispatch_get_main_queue(),^{
                [UIView transitionWithView:self.postedImageOutlet duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    [self.postedImageOutlet setImage:image];
                }completion:NULL];
            });
        } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        
    }
    
}

@end
