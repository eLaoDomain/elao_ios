//
//  ListingCollectionViewCell.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


@interface ListingCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIView *featuredView;
@property (weak, nonatomic) IBOutlet UIImageView *postedImageOutlet;


-(void)setProducts:(ProductDetails *)product;

@end
