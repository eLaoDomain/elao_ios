//
//  CellForFilteredItem.m
//
//  Created by Rahul Sharma on 06/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForFilteredItem.h"

@implementation CellForFilteredItem

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    
    self.buttonToDisplayFilter.layer.borderColor = mBaseColor.CGColor;
}

@end
