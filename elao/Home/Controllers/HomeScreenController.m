
//  HomeScreenController.h
//  Created by Rahul Sharma on 4/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//
#import "HomeScreenController.h"
#import "ListingCollectionViewCell.h"
#import "SVPullToRefresh.h"
#import "ProductDetailsViewController.h"
#import "CellForFilteredItem.h"
#import "FilterViewController.h"
#import "RFQuiltLayout.h"
#import "ActivityViewController.h"
#import "CameraViewController.h"
#import "SearchPostsViewController.h"
#import "ZoomInteractiveTransition.h"
#import "ZoomTransitionProtocol.h"
#import "StartBrowsingViewController.h"
#import "AskPermissionViewController.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "ProductDetails.h"
#define FiltersViewHeight 60


@import Firebase;

@interface HomeScreenController ()<WebServiceHandlerDelegate,RFQuiltLayoutDelegate,GetCurrentLocationDelegate,ZoomTransitionProtocol , SDWebImageManagerDelegate , AskPermissionDelegate, CLLocationManagerDelegate ,ProductDetailsDelegate,UICollectionViewDataSourcePrefetching >

@end

@implementation HomeScreenController

/*--------------------------------------*/
#pragma mark
#pragma mark - ViewController LifCycle
/*--------------------------------------*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.transition = [[ZoomInteractiveTransition alloc] initWithNavigationController:self.navigationController];
    [CommonMethods setNegativeSpacingforNavigationItem:self.navigationItem andExtraBarItem:self.searchButtonOutlet];
    minPrice = @""; maxPrice = @""; postedWithin = @""; sortBy = @"";
    dist = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    self.leadingConstraint = 40;
    self.FilterViewTopConstraint.constant= -FiltersViewHeight;
    currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
    currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    arrayOfcategory = [[NSMutableArray alloc]init];
    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
    productsArray = [[NSMutableArray alloc]init];
    [self addingActivityIndicatorToCollectionViewBackGround];
    [self RFQuiltLayoutIntialization];
    [self addingRefreshControl];
    [self notficationObservers];
    [self getCategoriesListFromServer];
    [self checkPermissionForAllowLocation];
    
    if([[Helper userToken] isEqualToString:mGuestToken]){
        NSDictionary *param = [CommonMethods updateDeviceDetailsForAdmin];
        [WebServiceHandler logGuestUserDevice:param andDelegate:self];
    }
    
    self.collectionViewOutlet.prefetchDataSource = self ;
    self.collectionViewOutlet.prefetchingEnabled = YES ;
 }


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.isScreenDidAppear = NO;
    self.sellStuffButtonView.hidden = NO;
    self.sellStuffButtonView.alpha = 0 ;
    self.bottomConstraintOfSellButton.constant = 60;
    
    NSDictionary *checkAdsCampaign  = [[NSUserDefaults standardUserDefaults] objectForKey:mAdsCampaignKey];
    if(checkAdsCampaign)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:mAdsCampaignKey];
        [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:checkAdsCampaign];
    }
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"openActivity"]) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openActivity"];
        [self.tabBarController setSelectedIndex:0];
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    [self getnotificationCount];
    [self showWelcomeMessageForBrowsing];
    
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"recent_login"])
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"recent_login"];
        [self requesForProductListings];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.shadowImage = nil;
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"isInstagramSharing"])
    {
        [_dic presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isInstagramSharing"];
    }
    [UIView transitionWithView:self.sellStuffButtonView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.sellStuffButtonView.alpha = 1.0 ;
                    }
                    completion:NULL];
    
    self.isScreenDidAppear = YES ;
    
}


/**
 Clear cache memory on warning.
 */
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache clearMemory];
    [imageCache clearDisk];
}

/**
 Deallocate memory again.
 */
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:mDeletePostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingAgainNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:mUpdatePostDataNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"openActivityScreen"];
    [[NSNotificationCenter defaultCenter] removeObserver:mSellingPostNotifiName];
    [[NSNotificationCenter defaultCenter] removeObserver:mShowAdsCampaign];
}


-(void)openActivityScreen{
    [self openActivityScreenForpush];
    
}


#pragma mark -
#pragma mark - Notification Observer

-(void)notficationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePostFromNotification:) name:mDeletePostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sellingPostAgain:) name:mSellingAgainNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(UpdatePostOnEditing:) name:mUpdatePostDataNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData:) name:mUpdatePromotedPost object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewPost:)name:mSellingPostNotifiName object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openActivityScreen)name:@"openActivityScreen" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShareOnInstagram:)name:@"instagramShare" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postNotificationToAppdelegate:)name:mShowAdsCampaign object:nil];
}



#pragma mark
#pragma mark - Location Permission & Delegate -

-(void)checkPermissionForAllowLocation
{
    BOOL locationServiceEnable ;
    if ([CLLocationManager locationServicesEnabled]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            locationServiceEnable = YES;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            locationServiceEnable = NO;
            AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
            askVC.permissionDelegate = self;
            askVC.locationPermission = YES ;
            askVC.locationEnable = locationServiceEnable ;
            [self.navigationController presentViewController:askVC animated:NO completion:nil];
        }
        else {
            [self allowPermission:YES];
        }
    }
    else {
        AskPermissionViewController *askVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AskPermissionStoryboardId"];
        askVC.permissionDelegate = self;
        askVC.locationPermission = YES ;
        askVC.LocationPrivacy = YES ;
        [self.navigationController presentViewController:askVC animated:NO completion:nil];
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {

    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self allowPermission:YES];
        
    }
    else
    {
        [self allowPermission:NO];
    }
}

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    self.latForFilter = latitude;
    self.longiForFilter = longitude;
    self.currentLat = latitude ;
    self.currentLong = longitude ;
    if(self.flagForLocation)
    {
        self.flagForLocation  = NO;
        [self requesForProductListings];
    }
}

- (void)updatedAddress:(NSString *)currentAddress
{
    

}

#pragma mark -
#pragma mark - RFQuiltLayout Intialization -

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.view.frame.size.width == 375)
    {
      layout.blockPixels = CGSizeMake( 37,31);
    }
    
    else if(self.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    else
    {
       layout.blockPixels = CGSizeMake( 79,31);
    }
    
    _collectionViewOutlet.collectionViewLayout = layout;
    layout.delegate=self;
    
}



#pragma mark
#pragma mark - Product Filter -

/**
 This is Delegate method of Filter VC.
 
 */
-(void)getFilteredItems:(NSMutableDictionary *)dicOfFilters miniPrice:(NSString *)min maxPrice:(NSString *)max curncyCode:(NSString *)currCode curncySymbol:(NSString *)currSymbol anddistance:(int)distnce
{
    minPrice = min;
    maxPrice = max;
    dist = distnce;
    currencyCode = currCode;
    currencySymbol = currSymbol;
    arrayOfcategory = dicOfFilters[@"category"];
    temp = [[NSMutableArray alloc] init];
    for (int i = 0 ;i <arrayOfcategory.count;i++) {
        NSString *typeOfFilter = @"category";
        NSString *valueForTheFilter = flStrForObj(arrayOfcategory[i]);
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:typeOfFilter forKey:@"typeOfFilter"];
        [cbmc setValue:valueForTheFilter forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"sortBy"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"sortBy" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"sortBy"] forKey:@"value"];
        [temp addObject:cbmc];
        
        NSString *tempString = [dicOfFilters valueForKey:@"sortBy"];
        [self handleSortByString:tempString];
        
        
    }
    
    if ([dicOfFilters valueForKey:@"postedWithin"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"postedWithin" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"postedWithin"] forKey:@"value"];
        [temp addObject:cbmc];
         NSString *tempString = [dicOfFilters valueForKey:@"postedWithin"];
        [self handleSortByString:tempString];

    }
    
    if ([dicOfFilters valueForKey:@"price"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"price" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"price"] forKey:@"value"];
        [temp addObject:cbmc];
    }
    
    if ([dicOfFilters valueForKey:@"distance"]) {
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"distance" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"distance"] forKey:@"value"];
        [temp addObject:cbmc];
    }
      self.leadingConstraint = [[dicOfFilters valueForKey:@"leadingConstraint"]integerValue];
    
    if([dicOfFilters valueForKey:@"locationName"]){
        NSMutableDictionary  *cbmc = [[NSMutableDictionary alloc] init];
        [cbmc setValue:@"locationName" forKey:@"typeOfFilter"];
        [cbmc setValue:dicOfFilters[@"locationName"] forKey:@"value"];
        [temp addObject:cbmc];
        self.locationName = [dicOfFilters valueForKey:@"locationName"];
        self.latForFilter = [[dicOfFilters valueForKey:@"lat"]doubleValue];
        self.longiForFilter = [[dicOfFilters valueForKey:@"long"]doubleValue];
    }
    [productsArray removeAllObjects];
    [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    if(temp.count>0){
        self.FilterViewTopConstraint.constant=0;
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
    else{
        [self requesForProductListings];
        self.FilterViewTopConstraint.constant = -FiltersViewHeight;
        currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
        currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    }
    [_collectionviewFilterItem reloadData];
    

}


-(void)handleSortByString:(NSString *)checkString
{
    
    if([checkString containsString: NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst ) ])
    {
        sortBy = @"postedOnDesc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst) ])
    {
        sortBy  = @"distanceAsc";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh) ])
    {
        sortBy = @"priceAsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow) ])
    {
        sortBy = @"priceDsc" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h) ])
    {
        postedWithin = @"1" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d) ])
    {
        postedWithin = @"7" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d) ])
    {
        postedWithin = @"30" ;
    }
    
    
}


-(void)handleRemoveFilters:(NSString *)checkString
{
    if([checkString containsString:NSLocalizedString(sortingByNewestFirst,sortingByNewestFirst )])
    {
        sortBy = @"";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByClosestFirst, sortingByClosestFirst)])
    {
        sortBy  = @"";
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceLowToHigh, sortingByPriceLowToHigh)])
    {
        sortBy = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(sortingByPriceHighToLow, sortingByPriceHighToLow)])
    {
        sortBy = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast24h, postedWithLast24h)])
    {
        postedWithin = @"" ;
    }
    if([checkString containsString:NSLocalizedString(postedWithLast7d, postedWithLast7d)])
    {
        postedWithin = @"" ;
    }
    
    if([checkString containsString:NSLocalizedString(postedWithLast30d,postedWithLast30d)])
    {
        postedWithin = @"" ;
    }
    
    
}






#pragma mark -
#pragma mark - Apply Filters
/**
 Apply search on Products by applying selected Filters.
 */
-(void)applyFiltersOnProductsWithIndex :(NSInteger)checkIndex
{
    
    if(self.locationName.length < 1)
    {
        self.locationName = getLocation.currentCity;
        self.latForFilter = self.currentLat;
        self.longiForFilter = self.currentLong;
    }

    categoryInStringFormat = [arrayOfcategory componentsJoinedByString:@","]; /** string with all categories**/
    
    NSDictionary *requestDic = @{mauthToken :flStrForObj([Helper userToken]),
                                 mlimit :@"20",
                                 moffset : flStrForObj([NSNumber numberWithInteger:checkIndex *20]),
                                 mSearchCategory  : flStrForObj(categoryInStringFormat),
                                 mSortby :   sortBy,
                                 mCurrency:  currencyCode,
                                 mMinPrice  : minPrice,
                                 mMaxPrice  : maxPrice,
                                 mPostedWithIn : postedWithin,
                                 mDistance  : [NSString stringWithFormat:@"%f",dist],
                                 mlocation  : flStrForObj(self.locationName),
                                 mlatitude  : [NSString stringWithFormat:@"%lf",self.latForFilter],
                                 mlongitude : [NSString stringWithFormat:@"%lf",self.longiForFilter],
                                 };
    
    [WebServiceHandler searchProductsByFilters:requestDic andDelegate:self];
}

#pragma mark -
#pragma mark - Remove Filters
/**
 Remove filter For Product.
 
 @param sender delete Filter button.
 */
- (IBAction)removeProductFilterButtonAcion:(id)sender {
    UIButton *btn=(UIButton *)sender;
    NSInteger tag= [btn tag]%100;
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"category"])
        [arrayOfcategory removeObjectAtIndex:tag];
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"distance"])
    {
        dist = 0;
        self.leadingConstraint = 40;
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"price"])
    {
        minPrice = @"";
        maxPrice = @"";
        
    }
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"sortBy"] || [temp[tag][@"typeOfFilter"] isEqualToString:@"postedWithin"])
    {
        [self handleRemoveFilters:temp[tag][@"value"]];
    }
    
    if([temp[tag][@"typeOfFilter"] isEqualToString:@"locationName"])
    {
    self.locationName = @"";
    self.latForFilter = 0;
    self.longiForFilter = 0;
    }
    
    [temp removeObjectAtIndex:tag];
    [productsArray removeAllObjects];
     [self reloadCollectionView] ;
    [avForCollectionView startAnimating];

    
    if (temp.count ==0) {
        [UIView animateWithDuration:0.4 animations:^{
            self.FilterViewTopConstraint.constant = -FiltersViewHeight;
            [self.view layoutIfNeeded];
        }];
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self requestForExplorePosts:self.currentIndex];
        currencyCode = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode]];
        currencySymbol = [NSString stringWithFormat:@"%@",[[NSLocale currentLocale] objectForKey: NSLocaleCurrencySymbol]];
    }
    else{
        self.currentIndex = 0;
        self.paging = 1;
        self.cellDisplayedIndex = -1;
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
        [self.collectionviewFilterItem reloadData];
 
   }


#pragma mrk -
#pragma mark - Pull To refresh -

-(void)addingRefreshControl {
    refreshControl = [[UIRefreshControl alloc]init];
    [self.collectionViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
}
-(void)refreshData:(id)sender {
    [self getnotificationCount];
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    if(temp.count==0){
       [self allowPermission:YES];
    }
    else{
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
    }
}
- (void)stopAnimation {
    __weak HomeScreenController *weakSelf = self;
    int64_t delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [weakSelf.collectionViewOutlet.pullToRefreshView stopAnimating];
        [weakSelf.collectionViewOutlet.infiniteScrollingView stopAnimating];
    });
}


#pragma mark -
#pragma mark - collectionview delegates -


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if(collectionView==_collectionviewFilterItem)
    {
        return temp.count;
        
    }
    return  productsArray.count;
    
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row +12 == productsArray.count && self.paging!= self.currentIndex && self.cellDisplayedIndex != indexPath.row) {
        self.cellDisplayedIndex = indexPath.row ;
        if(temp.count)
        {
        [self applyFiltersOnProductsWithIndex:self.currentIndex];
        }
        else
        {
        [self requestForExplorePosts:self.currentIndex];
        }
    }
    
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView==_collectionviewFilterItem)
    {
        CellForFilteredItem *cell=[collectionView dequeueReusableCellWithReuseIdentifier:mProductFiltersCellId forIndexPath:indexPath];
       [ cell.buttonToDisplayFilter setTitle: flStrForObj(temp[indexPath.row][@"value"]) forState:UIControlStateNormal];
        cell.deleteButton.tag = 100 + indexPath.row;
        return cell;
    }
    ListingCollectionViewCell  *collectionViewCell = [collectionView dequeueReusableCellWithReuseIdentifier:mSearchCollectioncellIdentifier forIndexPath:indexPath];
    if (collectionViewCell == nil) {
        collectionViewCell.postedImageOutlet = nil ;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [collectionViewCell setProducts:productsArray[indexPath.row]];
        });
    }
    return collectionViewCell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CellForFilteredItem *cell=(CellForFilteredItem*)[collectionView cellForItemAtIndexPath:indexPath];
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,cell.buttonToDisplayFilter.frame.size.height)];
    lbl.font=cell.buttonToDisplayFilter.titleLabel.font;
    if(temp.count)
    {
    lbl.text=flStrForObj(temp[indexPath.row][@"value"]);
    }
    CGFloat labelWidth = [CommonMethods measureWidthLabel:lbl];
    CGFloat cellWidth= labelWidth + 50;
    if(cellWidth >200){
        cellWidth = 200;
        return CGSizeMake(cellWidth,50 );
    }
    return CGSizeMake(cellWidth,50 );
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:indexPath];
    if([collectionView isEqual:self.collectionViewOutlet]) {
        ProductDetails *product = productsArray[indexPath.row];
        ProductDetailsViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:mInstaTableVcStoryBoardId];
        newView.indexPath = indexPath ;
        newView.productDelegate = self ;
        newView.hidesBottomBarWhenPushed = YES;
        newView.postId = product.postId;
        newView.dataFromHomeScreen = YES;
        newView.currentCity = getLocation.currentCity;
        newView.countryShortName = getLocation.countryShortCode ;
        newView.currentLattitude = [NSString stringWithFormat:@"%lf",self.currentLat];
        newView.currentLongitude = [NSString stringWithFormat:@"%lf",self.currentLong];
        newView.movetoRowNumber = indexPath.item;
        newView.imageFromHome = cell.postedImageOutlet.image;
        newView.product = product;
        [self.navigationController pushViewController:newView animated:YES];
    }
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5,5,5,5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
{
    return 0;
}

#pragma mark -
#pragma mark – RFQuiltLayoutDelegate -

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return [Helper blockSizeOfProduct:productsArray[indexPath.row] view:self.view];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}


#pragma mark -
#pragma mark - Request For Listings -

-(void)requesForProductListings
{
    self.currentIndex = 0;
    self.paging = 1;
    self.cellDisplayedIndex = -1;
    [self requestForExplorePosts:0];
}

-(void)requestForExplorePosts:(NSInteger )receivedindex {
    
    NSString *currentLattitudeParam,*currentLongitudeParam ;
    currentLattitudeParam = [NSString stringWithFormat:@"%lf",self.currentLat];
    currentLongitudeParam = [NSString stringWithFormat:@"%lf",self.currentLong];
    if(self.currentLat == 0)
    {
        currentLattitudeParam = @"";
        currentLongitudeParam = @"";
    }
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:mdeviceToken];
    if (token.length>0) {
        if ([[Helper userToken] isEqualToString:mGuestToken]) {
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                          mPushTokenKey : token,
                                          mlatitude : currentLattitudeParam,
                                          mlongitude : currentLongitudeParam
                                          };
            [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
        }
        else{
            NSDictionary *requestDict = @{
                                          mauthToken :flStrForObj([Helper userToken]),
                                          moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                          mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                          mlatitude : currentLattitudeParam,
                                          mlongitude: currentLongitudeParam,
                                          mPushTokenKey : token
                                          };
            [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
        }

    } else{
    
    if ([[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex * mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger:mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude : currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePostsForGuest:requestDict andDelegate:self];
    }
    else{
        NSDictionary *requestDict = @{
                                      mauthToken :flStrForObj([Helper userToken]),
                                      moffset:flStrForObj([NSNumber numberWithInteger:receivedindex* mPagingValue]),
                                      mlimit:flStrForObj([NSNumber numberWithInteger: mPagingValue]),
                                      mlatitude : currentLattitudeParam,
                                      mlongitude: currentLongitudeParam
                                      };
        [WebServiceHandler getExplorePosts:requestDict andDelegate:self];
    }
    }
}


#pragma mark -
#pragma mark - WebServiceDelegate -



-(void)internetIsNotAvailable:(RequestType)requsetType {
    [avForCollectionView stopAnimating];
    [refreshControl endRefreshing];
    if(!productsArray.count)
    {
        self.collectionViewOutlet.backgroundView = [Helper showMessageForNoInternet:YES forView:self.view];
    }
    
}

- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error {
    
    [Helper showMessageForNoInternet:NO forView:self.view];
    
    if (error) {
        [Helper showAlertWithTitle:NSLocalizedString(alertError, alertError) Message:NSLocalizedString(mCommonServerErrorMessage , mCommonServerErrorMessage) viewController:self];
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
        return;
    }
    else
    {
        [avForCollectionView stopAnimating];
        [refreshControl endRefreshing];
    }
    
    NSDictionary *responseDict = (NSDictionary*)response;
    
    switch (requestType) {
        case RequestTypeGetExploreposts:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    
                    [self handlingResponseOfExplorePosts:responseDict];
                }
                    break;
                case 204: {
                    if(!productsArray.count)
                    {
                        [self showingMessageForCollectionViewBackgroundForType:0];
                    }
                }
                    break;
                    
                    
            }
        }
            break;
        case RequestTypeunseenNotificationCount:
        {
            [avForCollectionView stopAnimating];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSString *unseenCount = flStrForObj(response[@"data"]);
                    if ([unseenCount isEqualToString:@"0"]) {
                        self.labelNotificationsCount.hidden = YES;
                    }
                    else
                    {
                        self.labelNotificationsCount.hidden = NO;
                        self.labelNotificationsCount.text = unseenCount ;
                    }
                }
                    break;
                    
            }
        }
            break ;
        
        case RequestTypeSearchProductsByFilters:
        {
            [[ProgressIndicator sharedInstance] hideProgressIndicator];
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    [self handlingResponseOfExplorePosts:response];
                }
                    break;
                case 204:{
                    if(self.cellDisplayedIndex == -1)
                    {
                        [productsArray removeAllObjects];
                         [self reloadCollectionView] ;
                        [self showingMessageForCollectionViewBackgroundForType:1];
                    }
                }
                    break;
            }
            
        }
            break ;
            
        case RequestTypeGetCategories:
        {
            switch ([responseDict[@"code"] integerValue]) {
                case 200: {
                    NSDictionary *categoryData = [[NSDictionary alloc]init];
                    categoryData = response[@"data"];
                    arrayOfCategoryImagesUrls = [[NSMutableArray alloc]init];
                    
                    for(NSDictionary *dic in categoryData)
                    {
                        [arrayOfCategoryImagesUrls addObject:dic[@"activeimage"]];
                        [arrayOfCategoryImagesUrls addObject:dic[@"deactiveimage"]];
                    }
                    [[NSUserDefaults standardUserDefaults] setObject:response[@"data"] forKey:mKeyForSavingCategoryList];
                }
                    break;
                default:
                    break;
            }
        }
            break ;
        default:
            break;
    }
}



#pragma mark -
#pragma mark - Handling Response -



-(void)handlingResponseOfExplorePosts :(NSDictionary *)response {
    self.collectionViewOutlet.backgroundView = nil;
    [refreshControl endRefreshing];
    if(self.currentIndex == 0) {
        [productsArray removeAllObjects];
        productsArray = [ProductDetails arrayOfProducts:response[@"data"]];
    }
    else {
        NSArray *productsWithPaging = [ProductDetails arrayOfProducts:response[@"data"]];
        [productsArray addObjectsFromArray:productsWithPaging];
        
    }
    self.currentIndex ++;
    self.paging ++;
    [self stopAnimation];
     [self reloadCollectionView] ;
}


-(void)addingActivityIndicatorToCollectionViewBackGround
{
    avForCollectionView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    avForCollectionView.frame =CGRectMake(self.view.frame.size.width/2 -12.5, self.view.frame.size.height/2 - 100, 25,25);
    avForCollectionView.tag  = 1;
    [self.collectionViewOutlet addSubview:avForCollectionView];
    [avForCollectionView startAnimating];
}


#pragma mark -
#pragma mark - Button Actions -


- (IBAction)sellStuffButton:(id)sender{
    if([[Helper userToken]isEqualToString:mGuestToken])
    {
        UINavigationController *navigationController = [CommonMethods presentLoginScreenController];
        [self presentViewController:navigationController animated:YES completion:nil];
        
    }
    else{
        
        [self openCameraScreen];
    }
}

- (IBAction)searchButtonAction:(id)sender {
    
    
    SearchPostsViewController *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:mSearchPostsID];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchVC];
    [self.navigationController presentViewController:navigationController  animated:YES completion:nil];
}

- (IBAction)notificationButtonACtion:(id)sender {
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        UINavigationController *nav = [CommonMethods presentLoginScreenController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.callBackForStartSelling = ^(BOOL isStartSelling) {
            if(isStartSelling)
            {
                [self openCameraScreen];
            }
        };
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

-(void)openActivityScreenForpush{
    if([[Helper userToken] isEqualToString:@"guestUser"]){
        
    }
    else{
        ActivityViewController *activityVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityControllerId"];
        activityVC.showOwnActivity = YES ;
        
        UINavigationController *nav  = [[UINavigationController alloc]initWithRootViewController:activityVC];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }
}

- (IBAction)filterButtonAction:(id)sender {
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    [prefetcher prefetchURLs:arrayOfCategoryImagesUrls progress:nil completed:^(NSUInteger completedNo, NSUInteger skippedNo) {
    }];
    FilterViewController *Fvc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterItemsStoryboardID"];
    Fvc.hidesBottomBarWhenPushed = YES;
    Fvc.delegate=self;
    Fvc.checkArrayOfFilters = YES;
    Fvc.minPrce = minPrice;
    Fvc.maxPrce = maxPrice;
    Fvc.distnce = dist;
    Fvc.arrayOfSelectedFilters = temp;
    Fvc.lattitude = self.latForFilter;
    Fvc.longitude = self.longiForFilter;
    Fvc.currencyCode = currencyCode ;
    Fvc.currencySymbol = currencySymbol;
    Fvc.leadingConstraint = self.leadingConstraint ;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:Fvc];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}


#pragma mark -
#pragma mark - No Posts View

-(void)showingMessageForCollectionViewBackgroundForType :(NSInteger )type{
    
    if(type == 0)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsNearYouText, noPostsNearYouText) ;
    }
    else if(type == 1)
    {
        self.emptyFilterTextLabel.text = NSLocalizedString(noPostsForAppliedFilter, noPostsForAppliedFilter) ;
    }
    UIView *backGroundViewForNoPost = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.collectionViewOutlet.frame.size.width,self.collectionViewOutlet.frame.size.height)];
    self.emtyFiltersView.frame = backGroundViewForNoPost.frame ;
    [backGroundViewForNoPost addSubview:self.emtyFiltersView];
    self.collectionViewOutlet.backgroundView = backGroundViewForNoPost;
}

#pragma mark -
#pragma mark - ZoomTransitionProtocol

-(UIView *)viewForZoomTransition:(BOOL)isSource
{
    NSIndexPath *selectedIndexPath = [[self.collectionViewOutlet indexPathsForSelectedItems] firstObject];
    ListingCollectionViewCell *cell = (ListingCollectionViewCell *)[self.collectionViewOutlet cellForItemAtIndexPath:selectedIndexPath];
    return cell.postedImageOutlet;
}


#pragma mark -
#pragma mark - Welcome Screen

-(void)showWelcomeMessageForBrowsing
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:mSignupFirstTime]){
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:mSignupFirstTime];
        [[NSUserDefaults standardUserDefaults]synchronize];
    self.tabBarController.tabBar.userInteractionEnabled = NO;
    StartBrowsingViewController *browsingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StartBrowsingStoryboardId"];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    browsingVC.callbackToActivateTab = ^(BOOL activate){
        self.tabBarController.tabBar.userInteractionEnabled = activate;
    };
    [self presentViewController:browsingVC animated:YES completion:nil];
    }
    else {
        self.tabBarController.tabBar.userInteractionEnabled = YES;

    }

}

#pragma mark -
#pragma mark - Tab Bar animation -

/* pass a param to describe the state change, an animated flag and a completion block matching UIView animations completion*/
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return (completion)? completion(YES) : nil;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    } completion:completion];
}

//Getter to know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}

#pragma mark -
#pragma mark - ScrollView Delegate -

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    scrollView = self.collectionViewOutlet;
    _currentOffset = self.collectionViewOutlet.contentOffset.y;
    lastScrollContentOffset = scrollView.contentOffset ;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isScreenDidAppear)
    {
    dispatch_async(dispatch_get_main_queue(),^{
    if (lastScrollContentOffset.y > scrollView.contentOffset.y) {
      // scroll up
         self.bottomConstraintOfSellButton.constant = 60;
         [self setTabBarVisible:YES animated:YES completion:nil];
         [[self navigationController] setNavigationBarHidden:NO animated:YES];
    }
    
    else if (lastScrollContentOffset.y < scrollView.contentOffset.y && lastScrollContentOffset.y >= 0 ) {
       // sroll down
        self.bottomConstraintOfSellButton.constant = 15;
        [self setTabBarVisible:NO animated:YES completion:nil];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
    }
    });
    }
}


#pragma mark -
#pragma mark - Notification Methods -

-(void)sellingPostAgain:(NSNotification *)noti
{
    
    
}

#pragma mark -
#pragma mark - Product Edited Notification -

-(void)UpdatePostOnEditing:(NSNotification *)noti
{
    [self requesForProductListings];
}

#pragma mark -
#pragma mark - Product Removed Notification -

-(void)deletePostFromNotification:(NSNotification *)noti {
    NSString *updatepostId = flStrForObj(noti.object[@"data"][@"postId"]);
    for (int i=0; i <productsArray.count;i++) {
        
        ProductDetails *product = productsArray[i];
        if ([product.postId isEqualToString:updatepostId])
        {
            [productsArray removeObjectAtIndex:i];
            [self reloadCollectionView] ;
            break;
        }
    }
}


#pragma mark -
#pragma mark - Product Added Notification -

-(void)addNewPost :(NSNotification *)noti
{
    ProductDetails *newProduct = [[ProductDetails alloc]initWithDictionary:noti.object[0]];
    [productsArray insertObject:newProduct atIndex:0];
    
    self.collectionViewOutlet.backgroundView = nil ;
     [self reloadCollectionView] ;
    
}


#pragma mark -
#pragma mark - Check Permission For Location -


/**
 This method is invoked after checking the permission for location services.
 On the basis of location services further server is requested for Listings.
 @param value Bool Value.
 */
-(void)allowPermission:(BOOL)value
{
    if(value)
    {
        //To get the current location
        getLocation = [GetCurrentLocation sharedInstance];
        [getLocation getLocation];
        getLocation.delegate = self;
        self.flagForLocation = YES;
    }
    else
    {
        [self requesForProductListings];
    }
    
}

#pragma mark-
#pragma mark - ProductDetails Delegate -


/**
 If product is removed.This delegate method get invoked.

 @param indexpath indexpath row.
 */
-(void)productIsRemovedForIndex:(NSIndexPath *)indexpath
{
    [productsArray removeObjectAtIndex:indexpath.row];
     [self reloadCollectionView] ;
}

-(void)updateProductForClickCount:(ProductDetails *)updatedProduct forIndex:(NSIndexPath *)indexpath
{
    productsArray[indexpath.row] = updatedProduct ;
}
#pragma mark-
#pragma mark -  Share On Instagram -


/**
 Once Product is successfully posted.This method get invoked to ask the permission to share on instagram.
 @param noti url to share.
 */

-(void)ShareOnInstagram:(NSNotification *)noti
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram:app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        NSString *path = [Helper instagramSharing:noti.object];
        _dic = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:path]];
        _dic.UTI = @"com.instagram.exclusivegram";
        _dic.delegate = nil;
        NSString *sharedMessage = [NSString stringWithFormat:NSLocalizedString(sharedVia, sharedVia),@" %@",APP_NAME];
        _dic.annotation = [NSDictionary dictionaryWithObject:sharedMessage forKey:@"InstagramCaption"];
    }
    
}


#pragma mark -
#pragma mark - Pending Notifications -

-(void)getnotificationCount
{
    if(![[Helper userToken] isEqualToString:mGuestToken]) {
        NSDictionary *requestDict = @{
                                      @"token":[Helper userToken]
                                      };
        [WebServiceHandler getUsernNotificationCount:requestDict andDelegate:self];
    }
}


#pragma mark -
#pragma mark - Get Categories -

/**
 Prefectch The Category List From  Server for Product Filters.
 */
-(void)getCategoriesListFromServer
{
NSDictionary *requestDict = @{
                              mLimit : @"50",
                              moffset :@"0"
                              };
[WebServiceHandler getCategories:requestDict andDelegate:self];
}

#pragma mark -
#pragma mark - reload CollectionView -


/**
 Reload collectionView without Animation.
 */
-(void)reloadCollectionView{
    [UIView animateWithDuration:0 animations:^{
            [_collectionViewOutlet reloadData];
    }];
}

#pragma mark -
#pragma mark - Camera Screen -

-(void)openCameraScreen
{
    CameraViewController *cameraVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CameraStoryBoardID"];
    cameraVC.sellProduct = TRUE;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cameraVC];
    [self presentViewController:nav animated:NO completion:nil];
}



#pragma mark -
#pragma mark - Ads Campaign Notification


/**
 Notify the method in App delegate to open the ads campaign
 
 @param noti details of campaign in NSdictionary format.
 */
-(void)postNotificationToAppdelegate:(NSNotification *)noti
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:mTriggerCampaign object:noti.object];
}

@end
