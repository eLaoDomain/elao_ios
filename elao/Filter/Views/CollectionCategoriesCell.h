//
//  CollectionCategoriesCell.h

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CollectionCategoriesCell : UICollectionViewCell




@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelForCategory;

-(void)setValuesWithArray:(NSArray *)arrayOfCategory indexPath:(NSIndexPath *)NSIndexPath andIconsArray:(NSArray *)arrayOficons;
@end
