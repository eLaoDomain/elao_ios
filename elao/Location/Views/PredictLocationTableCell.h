//
//  PredictLocationTableCell.h

//
//  Created by Rahul Sharma on 24/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PredictLocationTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
