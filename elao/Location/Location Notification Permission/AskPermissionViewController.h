//
//  AskPermissionViewController.h

//
//  Created by Rahul Sharma on 03/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CLLocationManager.h>

@protocol AskPermissionDelegate <NSObject>

@required
-(void)allowPermission:(BOOL )value;

@end


@interface AskPermissionViewController : UIViewController
{  
    GetCurrentLocation *getLocation;
}
@property (nonatomic, strong) CLLocationManager * locationManager;
@property(strong,nonatomic)id<AskPermissionDelegate> permissionDelegate ;
@property BOOL locationPermission ,NotificationPermission, locationEnable , LocationPrivacy ;

- (IBAction)yesNotifyMeButtonAction:(id)sender;
- (IBAction)okayButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *titleOfPermission;
@property (strong, nonatomic) IBOutlet UILabel *messageBelowTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *allowButtonOutlet;

@end
