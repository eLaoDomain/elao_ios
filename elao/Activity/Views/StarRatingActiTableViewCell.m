//
//  StarRatingActiTableViewCell.m

//
//  Created by Imma Web Pvt Ltd on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "StarRatingActiTableViewCell.h"

@implementation StarRatingActiTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self layoutIfNeeded];
    self.sellerPic.layer.cornerRadius = self.sellerPic.frame.size.width / 2;
    self.sellerPic.clipsToBounds = YES;
    self.rateUserAction.layer.cornerRadius = 2.0f;
    self.rateUserAction.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
