//
//  MultipleImagesCell.m

//  Created by Rahul Sharma on 28/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "MultipleImagesCell.h"

@implementation MultipleImagesCell
-(void)updateCellObjectWithImage:(UIImage *)Image;
{
    self.imageViewContainer.image = Image;
    self.removeButton.hidden=YES;
}

-(void)updateCellWithImage:(UIImage *)img andIndex:(NSIndexPath *)indexPath
{
//    self.imageViewContainer.tag=1000 + indexPath.row;
    self.imageViewContainer.image = img;
    self.removeButton.hidden=NO;
    self.removeButton.tag = 1000+indexPath.row;
}
@end
