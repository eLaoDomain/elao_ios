//
//  SuccessFullyPostedViewController.m

//
//  Created by Rahul Sharma on 10/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "SuccessFullyPostedViewController.h"
@import Firebase ;
@interface SuccessFullyPostedViewController ()

@end

@implementation SuccessFullyPostedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_isTwitterSharing)
    {
        [self.activityLoaderOutlet startAnimating];
        [self shareLinkOnTwitter:self.product];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES ;
}

-(void)shareLinkOnTwitter :(ProductDetails *)product
{
//    NSString * postLink = [SHARE_LINK  stringByAppendingString:[NSString stringWithFormat:@"%@",self.postId]];
//    NSString *mediaLink =[Helper getWebLinkForFeed:postLink];
//    NSString *posttext = [NSString stringWithFormat:@"Shared via @elao %@",mediaLink];
//
    NSString *url =  [SHARE_LINK stringByAppendingString:[NSString stringWithFormat:@"%@",product.postId]];
    
    NSString *originalLink = [SHARE_LINK  stringByAppendingString:product.postId] ;
    NSURL *link = [NSURL URLWithString:originalLink];
    FIRDynamicLinkComponents *components =
    [FIRDynamicLinkComponents componentsWithLink:link
                                          domain:mDomainForDeepLinking];
    
    FIRDynamicLinkSocialMetaTagParameters *socialParams = [FIRDynamicLinkSocialMetaTagParameters parameters];
    socialParams.title = product.productName ;
    socialParams.descriptionText = product.productDescription ;
    socialParams.imageURL = [NSURL URLWithString:product.mainUrl] ;
    components.socialMetaTagParameters = socialParams;
    
    
    FIRDynamicLinkIOSParameters *iosParams = [FIRDynamicLinkIOSParameters parametersWithBundleID:mIosBundleId];
    iosParams.fallbackURL = [NSURL URLWithString: url] ;
    components.iOSParameters = iosParams;
    
    FIRDynamicLinkAndroidParameters *androidParam = [FIRDynamicLinkAndroidParameters parametersWithPackageName:mAndroidBundleId];
    androidParam.fallbackURL = [NSURL URLWithString: url] ;
    components.androidParameters = androidParam;
    
    FIRDynamicLinkNavigationInfoParameters *navigationInfoParameters = [FIRDynamicLinkNavigationInfoParameters parameters];
    navigationInfoParameters.forcedRedirectEnabled = 0;
    components.navigationInfoParameters = navigationInfoParameters;
    
    [components shortenWithCompletion:^(NSURL *_Nullable shortURL,
                                        NSArray *_Nullable warnings,
                                        NSError *_Nullable error) {
        // Handle shortURL or error.
        if (error) {
            NSLog(@"Error generating short link: %@", error.description);
            return;
        }
        NSURL *shortenURL =  shortURL;
        NSArray *items = @[shortenURL];
        // build an activity view controller
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
        // and present it
        [Helper presentActivityController:controller forViewController:self];
        [self.activityLoaderOutlet stopAnimating];
        
    }];


}


- (IBAction)okaButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
}

- (IBAction)closeButtonAction:(id)sender {
    
    if(self.postingDelegate)
    {
        [_postingDelegate postingListedSuccessfully];
    }
    
    
}
@end

