//
//  CellForProdSummry.m

//
//  Created by Rahul Sharma on 13/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "CellForProdSummry.h"


@implementation CellForProdSummry

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


/**
 Update Fields in cell with data.

 @param dataArray.
 */
-(void)updateFieldsForProductWithDataArray :(ProductDetails *)product
{
    self.productName.text = product.productName ;
    self.productName.text = [self.productName.text capitalizedString];
    self.productType.text=  product.category ;
    self.productType.text = [self.productType.text capitalizedString];
    NSString *timeStamp = [Helper convertEpochToNormalTimeInshort:product.postedOn];
    self.labelForTimeStamp.text = timeStamp;
}

@end
