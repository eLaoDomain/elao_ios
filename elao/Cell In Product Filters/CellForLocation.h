
//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForLocation : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *selectedLocLab;
@property (strong, nonatomic) IBOutlet UILabel *LabelForChangeLocationTitle;

@end
